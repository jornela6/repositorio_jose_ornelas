﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_II_Interfaces
{
    interface Transporte
    {
        string marca { get; set; }
        string tipo { get; set; }
        void acelerar ();
        void frenar ();
        void girar ();
    }
}
