﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_II_Interfaces
{
    class Automovil : Transporte
    {
        public string marca { get; set; }
        public string tipo { get; set; }
        public Automovil(string marca, string tipo)
        {
            this.marca = marca;
            this.tipo = tipo;
        }
        public void acelerar()
        {
            Console.WriteLine("Acelerando");
        }
        public void frenar()
        {
            Console.WriteLine("Frenando");
        }
        public void girar()
        {
            Console.WriteLine("Girando");
        }
    }
}
