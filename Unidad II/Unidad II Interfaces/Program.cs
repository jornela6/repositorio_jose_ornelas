﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_II_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Automovil[] arreglocarros = new Automovil[6]
            {
                new Automovil("Ford", "Sedan"),
                new Automovil("Chevrolet" , "Camioneta"),
                new Automovil("Audi", "Coupe"),
                new Automovil("Dodge" , "Compacto"),
                new Automovil("Vw", "Sedan"),
                new Automovil("Renault" , "Sub"),
            };
            Console.WriteLine("******Arreglo Vehiculos*******");
            foreach (Automovil C in arreglocarros)
            {
                Console.WriteLine("La marca es {0} y el tipo es {1}", C.marca, C.tipo);
            }
            Console.WriteLine("Presione cualquier tecla para continuar");
            Console.ReadKey();
        }
    }
}
