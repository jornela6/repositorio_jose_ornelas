﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_II___Polimorfismo
{
    class Program
    {
        static void Main(string[] args)
        {
            Vehicle vehicle = new Vehicle();
            vehicle.Display();      
            vehicle = new Ducati();
            vehicle.Display();
            vehicle = new Lamborghini();
            vehicle.Display();
            Console.ReadKey();
        }
        public class Vehicle
        {
            protected int NumberOfWheels { get; set; } = 0;
            public Vehicle()
            {
            }

            public virtual void Display()
            {
            }
        }

        public class Ducati : Vehicle
        {
            public Ducati()
            {
                NumberOfWheels = 2;
            }

            public override void Display()
            {
                Console.WriteLine($"Numero de llantas para {nameof(Ducati)} is {NumberOfWheels}");
            }
        }

        public class Lamborghini : Vehicle
        {
            public Lamborghini()
            {
                NumberOfWheels = 4;
            }

            public override void Display()
            {
                Console.WriteLine($"Numero de llantas para {nameof(Lamborghini)} is {NumberOfWheels}");
            }
        }

    }
}
