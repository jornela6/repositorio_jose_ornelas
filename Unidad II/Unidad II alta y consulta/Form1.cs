﻿using ServiceStack.DataAnnotations;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unidad_II_alta_y_consulta
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                var dbFactory = new OrmLiteConnectionFactory("Server=localhost; Uid=root;", MySqlDialect.Provider);
                using (var db = dbFactory.Open())
                {
                    db.ExecuteSql("CREATE DATABASE IF NOT EXISTS `trabajadores`; USE `trabajadores`;");
                    db.CreateTable<TrabajadoresDeFord>();
                    var ne = db.Save(
                        new TrabajadoresDeFord
                    {
                        NombreCompleto = textBox1.Text,
                        Sector = textBox2.Text
                    });
                    db.Close();
                }
            }
            catch (Exception error)
            {
                Console.WriteLine();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dbFactory = new OrmLiteConnectionFactory("Server=localhost; Uid=root;", MySqlDialect.Provider);
            using (var db = dbFactory.Open())
            {
                db.ExecuteSql("CREATE DATABASE IF NOT EXISTS `trabajadores`; USE `trabajadores`;");
                foreach (var x in db.Select<TrabajadoresDeFord>())
                {
                    richTextBox1.AppendText($"{x.ToString()} {Environment.NewLine}");
                }
                db.Close();
            }
        }
    }
    [Table("trabajadoresdeford")]
    public class TrabajadoresDeFord
    {
        [PrimaryKey, AutoIncrement, Alias("id")]
        public int Id { get; set; }
        [Alias("nombre_completo"), StringLength(50)]
        public string NombreCompleto { get; set; }

        [Alias("sector")]
        public string Sector { get; set; }
        public override string ToString()
        {
            return $"Id:[{Id}], Nombre Completo:[{NombreCompleto}], Sector:[{Sector}]";
        }
    }
}
