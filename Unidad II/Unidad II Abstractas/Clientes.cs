﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_II_Abstractas
{
    public abstract class Clientes
    {
        public abstract int Id { get; set; }
        public abstract string Nombre { get; set; }
        public abstract string RFC { get; set; }

    }
}
