﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_II_Abstractas
{
    public class ClienteBase : Clientes
    {
        public ClienteBase()
        {
            Id = 0;
            Nombre = string.Empty;
            RFC = string.Empty;
        }
        public ClienteBase(int pId, string pNombre, string pRFC)
        {
            Id = pId;
            Nombre = pNombre;
            RFC = pRFC;
        }
        public override int Id { get; set; }
        public override string Nombre { get; set; }
        public override string  RFC { get; set; }
    }
}
