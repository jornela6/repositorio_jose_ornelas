﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_II_Abstractas
{
    class Program
    {
        static void Main(string[] args)
        {
            Clientes cliente = new ClienteBase(0, "José Ornelas", "OEJR831220");
            Console.WriteLine(cliente.Id + " " + cliente.Nombre + " RFC:" + cliente.RFC);
            Console.ReadKey();
        }
    }
}
