﻿using ServiceStack.DataAnnotations;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unidad_II_baja_y_modificacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dbFactory = new OrmLiteConnectionFactory("Server=localhost; Uid=root;", MySqlDialect.Provider);
            using (var db = dbFactory.Open())
            {
                try
                {


                    db.Delete<TrabajadoresDeFord>(x => x.Id == Convert.ToInt32(textBox3.Text));
                }catch(Exception err)
                {
                    MessageBox.Show("No se borro");
                }
                db.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var dbFactory = new OrmLiteConnectionFactory("Server=localhost; Uid=root;", MySqlDialect.Provider);
            using (var db = dbFactory.Open())
            {
                try
                {
                   db.Update<TrabajadoresDeFord>(new TrabajadoresDeFord
                   {
                       Id = Convert.ToInt32(textBox3.Text),
                       NombreCompleto = textBox1.Text,
                       Sector = textBox2.Text
                   });
                }
                catch (Exception err)
                {
                    MessageBox.Show("No se actualizo");
                }
                db.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var dbFactory = new OrmLiteConnectionFactory("Server=localhost; Uid=root;", MySqlDialect.Provider);
            using (var db = dbFactory.Open())
            {
                db.ExecuteSql("CREATE DATABASE IF NOT EXISTS `trabajadores`; USE `trabajadores`;");
                foreach (var x in db.Select<TrabajadoresDeFord>())
                {
                    richTextBox1.AppendText($"{x.ToString()} {Environment.NewLine}");
                }
                db.Close();
            }
        }
    }
    [Table("trabajadoresdeford")]
    public class TrabajadoresDeFord
    {
        [PrimaryKey, AutoIncrement, Alias("id")]
        public int Id { get; set; }
        [Alias("nombre_completo"), StringLength(50)]
        public string NombreCompleto { get; set; }

        [Alias("sector")]
        public string Sector { get; set; }
        public override string ToString()
        {
            return $"Id:[{Id}], Nombre Completo:[{NombreCompleto}], Sector:[{Sector}]";
        }
    }
}
