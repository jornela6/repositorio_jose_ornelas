﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unidad_II_conexion_MySql
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                var dbFactory = new OrmLiteConnectionFactory("Server=localhost; Uid=root;", MySqlDialect.Provider);
                using (var db = dbFactory.Open())
                {
                    MessageBox.Show(db.State.ToString());
                    db.Close();
                }
            }catch(Exception error)
            {

            }

        }
    }
}
