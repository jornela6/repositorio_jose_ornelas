﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_1_ciclos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("10 hola mundos");
            for (var x = 0; x <= 10; x++)
            {
                Console.WriteLine($"Hola mundo : {x}");
            }
            Console.ReadKey();
            Console.WriteLine("escribe exit");
            var e = Console.ReadLine();
            var boo = true;
            while (boo)
            {
                if (e == "exit")
                {
                    boo = false;
                }
                else
                {
                    Console.WriteLine("escribe exit");
                    e = Console.ReadLine();
                }
                Console.WriteLine(e);
            }
        }
    }
}
