﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_1_Variables_de_texto
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder text = new StringBuilder();
            Console.WriteLine("***JOSE RAFAEL ORNELAS*** ");
            Console.WriteLine("escribe 3 lineas de texto:");
            text.AppendLine(Console.ReadLine());
            text.AppendLine(Console.ReadLine());
            text.AppendLine(Console.ReadLine());
            Console.WriteLine($"Texto de las 3 lineas: [{text.ToString()}]");
            Console.ReadKey();
        }    
    }
}
