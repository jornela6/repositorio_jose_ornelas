﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_1_variables_y_operaciones_aritmeticas
{
    class Program
    {
        static void Main(string[] args)
        {
            var suma = 2 + 2;
            var resta = 3 - 2;
            var multiplicacion = 2 * 10;
            var division = 300 / 30;
            var residuo = 100 % 5;
            Console.WriteLine($"suma : {suma}");
            Console.WriteLine($"resta : {resta}");
            Console.WriteLine($"multiplicacion : {multiplicacion}");
            Console.WriteLine($"division : {division}");
            Console.WriteLine($"residuo : {residuo}");

            Console.ReadKey();
        }
    }
}
