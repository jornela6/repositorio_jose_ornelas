﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_I_switch_y_case
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello or Bye");
            var texto = Console.ReadLine();
            switch (texto)
            {
                case "hello": Console.WriteLine("hola en ingles"); break;
                case "bye": Console.WriteLine("adios en ingles"); break;
                default:
                    Console.WriteLine("no es ningun caso");
                    break;
            }
            Console.ReadKey();
        }
    }
}
