﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unidad_I_eventos_en_apps
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Jose");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Rafael");
        }

        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("doble click Jose Ornelas");
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            label1.Text = $"Cambio de tamaño la ventana {Guid.NewGuid().ToString()}";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
