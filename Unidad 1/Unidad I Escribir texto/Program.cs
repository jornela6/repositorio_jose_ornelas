﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_I_Escribir_texto
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("escribe 3 lineas");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(Console.ReadLine());
            stringBuilder.AppendLine(Console.ReadLine());
            stringBuilder.AppendLine(Console.ReadLine());
            File.WriteAllText("archivotexto.txt", stringBuilder.ToString());
            Console.ReadKey();
        }
    }
}
