﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_I_sentencia_IF_else
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("pon jacob:");
            var e = Console.ReadLine();
            if(e == "jacob")
            {
                Console.WriteLine("Si eres jacob");
            }
            else
            {
                Console.WriteLine($"No pusiste jacob, pusiste apellido XD: {e}");
            }
            Console.ReadKey();
        }
    }
}
