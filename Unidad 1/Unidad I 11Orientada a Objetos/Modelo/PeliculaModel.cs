﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_I_Orientada_a_Objetos.Modelo
{
    public  class PeliculaModel
    {
        public PeliculaModel()
        {

        }
        public PeliculaModel(string Nombre, string Genero, int Estreno)
        {
            this.Nombre = Nombre;
            this.Genero = Genero;
            this.Estreno = Estreno;
        }
        public string Nombre { get; set; }
        public string Genero { get; set; }
        public int Estreno { get; set; }
    }
}
