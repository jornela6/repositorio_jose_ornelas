﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unidad_I_Orientada_a_Objetos.Modelo;
namespace Unidad_I_Orientada_a_Objetos.Dato
{
    class Pelicula
    {
        List<PeliculaModel> Lista = new List<PeliculaModel>();
        public void Guardar(PeliculaModel modelo)
        {
            Lista.Add(modelo);
        }
        public List<PeliculaModel> Consultar()
        {
            return Lista;

        }
    }
}
