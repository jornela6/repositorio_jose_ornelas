﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unidad_I_Orientada_a_Objetos.Dato;
using Unidad_I_Orientada_a_Objetos.Modelo;

namespace Unidad_I_Orientada_a_Objetos
{
    class Program
    {
        static void Main(string[] args)
        {
            Ejecutar();
        }
        private static  void Ejecutar ()
        {
            Pelicula dato = new Pelicula();
            char opcion = ' ';
            do
            {
                PeliculaModel modelo = new PeliculaModel();
                Console.WriteLine("Escriba el nombre de la pelicula");
                modelo.Nombre = Console.ReadLine();
                Console.WriteLine("Escriba el genero de la pelicula");
                modelo.Genero = Console.ReadLine();
                Console.WriteLine("Escriba el año de la pelicula");
                modelo.Estreno = int.Parse(Console.ReadLine());
                dato.Guardar(modelo);
                Console.WriteLine("Desea añadir otra pelicula");
                opcion = char.Parse(Console.ReadLine());
            }
            while (opcion.Equals('s') || opcion.Equals('S'));
            foreach (PeliculaModel item in dato.Consultar())
            {
                Console.WriteLine("Nombre: "+ item.Nombre);
                Console.WriteLine("Genero: " + item.Genero);
                Console.WriteLine("Año de pelicula: " + item.Estreno);
                Console.WriteLine("***_____________________________________***");
            }
            Console.ReadLine(); 
        }
    }
}
