﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_III.__Patron_Observador
{
    class Program
    {
        static void Main(string[] args)
        {
            EstacionMetereologica estacionMetereologica = new EstacionMetereologica();

            DispositivoTiempoActual dispositivoTiempoActual = new DispositivoTiempoActual(estacionMetereologica);
            DispositivoEstadisticas dispositivoEstadisticas = new DispositivoEstadisticas(estacionMetereologica);
            DispositivoPredictivo dispositivoPredictivo = new DispositivoPredictivo(estacionMetereologica);

            estacionMetereologica.HaCambiadoElTiempo += dispositivoTiempoActual.ActualizarPantallaDipositivo;
            estacionMetereologica.HaCambiadoElTiempo += dispositivoEstadisticas.AñadirDatosParaLasEstadisticas;
            estacionMetereologica.HaCambiadoElTiempo += dispositivoPredictivo.AñadirDatosDePrediccion;

            estacionMetereologica.AumentarLaTemperaturaEnGrados(1);

            Console.ReadLine();
        }
    }
}
