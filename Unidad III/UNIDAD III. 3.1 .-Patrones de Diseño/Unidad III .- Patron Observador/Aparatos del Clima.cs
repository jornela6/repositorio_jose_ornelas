﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_III.__Patron_Observador
{
    public class DispositivoTiempoActual : IObservador
    {
        private readonly EstacionMetereologica _estacionMetereologica;

        public DispositivoTiempoActual(EstacionMetereologica estacionMetereologica)
        {
            _estacionMetereologica = estacionMetereologica;
        }
        public void ActualizarPantallaDipositivo(object sender, Tuple<decimal, decimal, decimal> medidas)
        {
            var temperatura = medidas.Item1;
            var presion = medidas.Item2;
            var humedad = medidas.Item3;

            // cosas importantes que hacer con las medidas
        }
        public void Actualizar()
        {
            // ... código

            // Cuando me vaya bien, llamo al publicador para conocer las medidas
            var temperatura = _estacionMetereologica.Temperatura;
            var presion = _estacionMetereologica.Presion;
            var humedad = _estacionMetereologica.Humedad;

            // ... código
        }
    }

    public class DispositivoEstadisticas : IObservador
    {
        private readonly EstacionMetereologica _estacionMetereologica;

        public DispositivoEstadisticas(EstacionMetereologica estacionMetereologica)
        {
            _estacionMetereologica = estacionMetereologica;
        }
        public void AñadirDatosParaLasEstadisticas(object sender, Tuple<decimal, decimal, decimal> medidas)
        {
            var temperatura = medidas.Item1;
            var presion = medidas.Item2;
            var humedad = medidas.Item3;

            // cosas importantes que hacer con las medidas
        }

        public void Actualizar()
        {
            // ... código

            // Cuando me vaya bien, llamo al publicador para conocer las medidas
            var temperatura = _estacionMetereologica.Temperatura;
            var presion = _estacionMetereologica.Presion;
            var humedad = _estacionMetereologica.Humedad;

            // ... código
        }
    }

    public class DispositivoPredictivo : IObservador
    {
        private readonly EstacionMetereologica _estacionMetereologica;

        public DispositivoPredictivo(EstacionMetereologica estacionMetereologica)
        {
            _estacionMetereologica = estacionMetereologica;
        }
        public void AñadirDatosDePrediccion(object sender, Tuple<decimal, decimal, decimal> medidas)
        {
            var temperatura = medidas.Item1;
            var presion = medidas.Item2;
            var humedad = medidas.Item3;

            // cosas importantes que hacer con las medidas
        }

        public void Actualizar()
        {
            // ... código

            // Cuando me vaya bien, llamo al publicador para conocer las medidas
            var temperatura = _estacionMetereologica.Temperatura;
            var presion = _estacionMetereologica.Presion;
            var humedad = _estacionMetereologica.Humedad;

            // ... código
        }
    }

    public class EstacionMetereologica
    {
        public decimal Temperatura { get; private set; }
        public decimal Presion { get; private set; }
        public decimal Humedad { get; private set; }

        public event EventHandler<Tuple<decimal, decimal, decimal>> HaCambiadoElTiempo;

        public void AumentarLaTemperaturaEnGrados(int grados)
        {
            Temperatura = grados + 1;

            Notificar();
        }

        public void Notificar()
        {
            var medidas = new Tuple<decimal, decimal, decimal>(Temperatura, Humedad, Presion);

            if (HaCambiadoElTiempo != null)
                HaCambiadoElTiempo.Invoke(this, medidas);
        }
    }

    public interface IPublicador
    {
        void RegistrarObservador(IObservador observador);
        void QuitarObservador(IObservador observador);

        void Notificar();
    }

    public interface IObservador
    {
        void Actualizar();
    }
}
