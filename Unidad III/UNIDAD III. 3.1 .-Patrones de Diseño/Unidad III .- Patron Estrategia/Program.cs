﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_III.__Patron_Estrategia
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PatoDomestico");
            var PatoDomestico = new PatoDomestico();
            PatoDomestico.Graznar();
            PatoDomestico.Mostrar();
            PatoDomestico.Nadar();
            Console.WriteLine("PatoDeJuguete");
            var PatoDeJuguete = new PatoDeJuguete();
            PatoDeJuguete.Graznar();
            PatoDeJuguete.Mostrar();
            PatoDeJuguete.Nadar();
            Console.ReadKey();
        }
    }
}
