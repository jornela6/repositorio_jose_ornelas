## Patrones de diseño.

###Patron Singleton.

*El patrón de diseño Singleton (instancia única) está diseñado para restringir la creación de objetos pertenecientes a una clase
o el valor de un tipo a un único objeto. Su intención consiste en garantizar que una clase sólo tenga una instancia y proporcionar 
un punto de acceso global a ella. No se encarga de la creación de objetos en sí, sino que se enfoca en la restricción en la creación 
de un objeto.*

**El uso del patrón Singleton proporciona los siguientes beneficios:**

- Reduce el espacio de nombres. El patrón es una mejora sobre las variables globales. Ya no se reservan nombres para las variables 
globales, ahora solo existen instancias.
- Controla el acceso a la instancia única, porque la clase Singleton encapsula la única instancia. Asi se obtiene control sobre cómo
y cuándo se accede a ella.
- Permite el refinamiento de las operaciones y la representación.
- Permite un numero variable de instancias. El patron es facilmente configurable para permitir más de una instancia.
- Más flexible que las operaciones de clases

**Desventajas**
- Hay que tener especial cuidado cuando el Singleton se utiliza en un ambiente multihilos, porque puede crear problemas si no se 
implementa de la manera adecuada.
- Es necesario usar mecanismos que aseguren la atomicidad del método getInstance.

###Patron Composite

* El patrón estructural Composite permite componer objetos en estructuras arbóreas para representar jerarquías de todo-parte, de modo 
que los clientes puedan tratar a los objetos individuales y a los compuestos de manera uniforme.*

**Ventajas**

- Permite jerarquías de objetos tan complejas como se quiera. Allá donde el cliente espere un objeto primitivo, podrá recibir un 
compuesto y no se dará cuenta
- Simplifica el cliente. Al eliminar el código para distinguir entre unos y otros.
- Se pueden añadir nuevos componentes fácilmente.

**Desventajas**
- Podría hacer el diseño demasiado general, especialmente cuando queremos restringir los componentes que pueden formar parte de un 
compuesto determinado.

###Patron Estrategia.

* Define una familia de algoritmos, encapsula cada uno de ellos y los hace intercambiables. Permite que un algoritmo varié 
independientemente de los clientes que lo usan.*

**Ventajas**

- Es más fácil de leer el código.
- Más fácil por tanto de mantener y por supuesto de ampliar con nuevas funcionalidades.
- Crear pequeñas clases que contienen un algoritmo muy concreto.

**Desventajas**

- Poco factible para sistemas de tiempo real.

###Patron factoria simple.

* El patrón de diseño Factory Method nos permite la creación de un subtipo determinado por medio de una clase de Factoría, 
la cual oculta los detalles de creación del objeto.
La intención del Factory Method es tener una clase a la cual delegar la responsabilidad de la creación de los objetos, para que no sea 
el mismo programador el que decida que clase instanciará, si no que delegará esta responsabilidad al Factory confiando en que este 
le regresará la clase adecuada para trabajar.

**Ventajas**

- Es muy simple.
- Separar la lógica de creación de objetos en una clase de factoria
- Facilidad de variar dinamicamnte la implementación del objeto fabricado sin afectar al cliente.

**Desventajas**

- Acoplamiento entre tu clase y la clase que crea la instancia. Ambas quedan ligadas para siempre.

###Patron observador

* es flexible y sencillo. Se utiliza cuando uno quiere notificar a otros objetos de un evento. En principio, lo que sucede es que 
un Objeto (llamemoslo Observador) se inscribe a otro Objeto (llamemoslo Sujeto) y este le avisa cuando un evento es disparado 
(o cuando el estado del Sujeto ha cambiado).

** Ventajas **
- La principal ventaja de este patrón es que todo se logra sin recurrir a un acoplamiento estrecho.

**Desventajas**

- Cuando un observador es demasiado grande puede traer consecuencias en el uso de memoria.
- Otra posible desventaja aparece cuando se inscribe un observador que a su vez sea un sujeto para otros objetos. Si la 
implementación no es limpia, será muy difícil corregir o encontrar errores cuando estos sucedan.
