﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_III___Patron_Compuesto
{
    public class Referencia
    {
        public Referencia(string nombre, decimal coste)
        {
            if (string.IsNullOrEmpty(nombre))
                throw new ArgumentException(nameof(nombre));

            if (coste < 0)
                throw new ArgumentException(nameof(coste));

            Nombre = nombre;
            Coste = coste;
        }
        public string Nombre { get; }
        public decimal Coste { get; }
    }

    public abstract class Componente
    {
        public abstract string Nombre { get; }

        public abstract void Añadir(Componente componente);

        public abstract void Quitar(Componente componente);

        public abstract decimal CalcularCoste(int nivel);
    }

    public class Pieza : Componente
    {
        private readonly Referencia _referencia;

        public Pieza(Referencia referencia)
        {
            _referencia = referencia;
        }

        public override string Nombre => _referencia.Nombre;

        public override void Añadir(Componente componente)
        {
            throw new NotImplementedException();
        }

        public override void Quitar(Componente componente)
        {
            throw new NotImplementedException();
        }

        public override decimal CalcularCoste(int nivel)
        {
            Console.WriteLine(new String('-', nivel) + " Pieza: " + Nombre + " - Coste:" + _referencia.Coste);

            return _referencia.Coste;
        }
    }

    public class Conjunto : Componente
    {
        private readonly Referencia _referencia;
        private readonly List<Componente> _subComponentes;


        public override string Nombre
        {
            get { return _referencia.Nombre; }
        }

        public override void Añadir(Componente componente)
        {
            _subComponentes.Add(componente);
        }

        public override void Quitar(Componente componente)
        {
            _subComponentes.Remove(componente);
        }

        public Conjunto(Referencia referencia)
        {
            _referencia = referencia;
            _subComponentes = new List<Componente>();
        }

        public void Añadir(int cantidad, Componente componente)
        {
            for (int i = 0; i < cantidad; i++)
            {
                Añadir(componente);
            }
        }


        public override decimal CalcularCoste(int nivel)
        {
            decimal coste = _referencia.Coste;

            Console.WriteLine(new String('-', nivel) + " " + Nombre + ": " + coste);

            foreach (var componenteProducto in _subComponentes)
            {
                coste = coste + componenteProducto.CalcularCoste(nivel + 1);
            }

            return coste;
        }
    }
}
