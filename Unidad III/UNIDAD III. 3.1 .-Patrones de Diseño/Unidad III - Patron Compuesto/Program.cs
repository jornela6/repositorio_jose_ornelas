﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_III___Patron_Compuesto
{
    class Program
    {
        static void Main(string[] args)
        {
            var referenciaB = new Referencia("B", coste: 4);
            var conjuntoB = new Conjunto(referenciaB);

            var referenciaB1 = new Referencia("B1", coste: 1);
            var piezaB1 = new Pieza(referenciaB1);

            var referenciaB2 = new Referencia("B2", coste: 2);
            var conjuntoB2 = new Conjunto(referenciaB2);

            var referenciaB21 = new Referencia("B21", coste: 1);
            var piezaB21 = new Pieza(referenciaB21);

            var referenciaB22 = new Referencia("B22", coste: 2);
            var piezaB22 = new Pieza(referenciaB22);

            conjuntoB2.Añadir(3, piezaB21);
            conjuntoB2.Añadir(2, piezaB22);

            conjuntoB.Añadir(5, piezaB1);
            conjuntoB.Añadir(3, conjuntoB2);

            // Hasta aquí he creado el conjuntoB

            // A continuación el cálculo del coste en el que solo 
            // es necesario una línea

            Console.WriteLine("Coste de " + conjuntoB.Nombre + ":");
            Console.WriteLine(conjuntoB.CalcularCoste(1));
            Console.ReadKey();
        }
    }
}
