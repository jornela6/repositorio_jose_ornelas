﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_III.Patron__Factoria_Simple
{
    public class Servicio
    {
        private string v;
        private Servicio2 servicio2;
        private Servicio3 servicio3;

        public Servicio(string v, Servicio2 servicio2, Servicio3 servicio3)
        {
            this.v = v;
            this.servicio2 = servicio2;
            this.servicio3 = servicio3;
        }
    }
    public class Servicio2
    {
        public Servicio2()
        {
            Console.WriteLine("Servicio 2");
        }
    }
    public class Servicio3
    {
        public Servicio3()
        {
            Console.WriteLine("Servicio 3");
        }
    }
    public static class FactoriaServicios
    {
        public static Servicio CrearServicio()
        {
            return new Servicio("cadena conexión", new Servicio2(), new Servicio3());
        }
    }
}
