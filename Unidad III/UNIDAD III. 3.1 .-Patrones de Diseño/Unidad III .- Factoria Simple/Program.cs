﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad_III.Patron__Factoria_Simple
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iniciamos los servicios");
            FactoriaServicios.CrearServicio();
            Console.ReadKey();
        }
    }
}
