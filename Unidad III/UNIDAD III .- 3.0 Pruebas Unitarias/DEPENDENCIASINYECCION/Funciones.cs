﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEPENDENCIASINYECCION
{
    public static class Funciones
    {
        public static string SUMAR_PALABRAS(string word1, string word2)
        {
            return $"{word1}{word2}";
        }
        public static double MULTIPLICAR(double valor1, double valor2)
        {
            return valor1 * valor2;
        }
    }
}
