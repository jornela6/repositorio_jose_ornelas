﻿using DEPENDENCIASINYECCION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UNIDAD_III.__3._0_Pruebas_Unitarias
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Probar sumar 2 palabras y multiplicar");
            Console.WriteLine(Funciones.SUMAR_PALABRAS("pedro ", "pako"));
            Console.WriteLine(Funciones.MULTIPLICAR(20,30));
            Console.ReadKey();
        }
    }
}
