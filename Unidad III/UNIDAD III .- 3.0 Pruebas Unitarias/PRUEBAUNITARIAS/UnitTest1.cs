﻿using System;
using DEPENDENCIASINYECCION;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PRUEBAUNITARIAS
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PROBAR_SUMA_PALABRAS()
        {
            Assert.AreEqual("pepepaco",Funciones.SUMAR_PALABRAS("pepe", "paco"));
        }

        [TestMethod]
        public void PROBAR_MULTIPLICAR()
        {
            Assert.AreEqual(6,Funciones.MULTIPLICAR(2, 3));
        }
    }
}
